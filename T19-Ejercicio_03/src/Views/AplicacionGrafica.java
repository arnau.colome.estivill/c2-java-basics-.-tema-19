package Views;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;


public class AplicacionGrafica extends JFrame {

	private JPanel contentPane;

	public AplicacionGrafica() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 582, 483);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		JLabel eSO = new JLabel("Elije un sistema Operativo");
		eSO.setBounds(39, 29, 164, 23);
		contentPane.add(eSO);
		JRadioButton rdbtnO_1 = new JRadioButton("Windows");
		rdbtnO_1.setBounds(39, 59, 109, 23);
		contentPane.add(rdbtnO_1);
		
		JRadioButton rdbtnO_2 = new JRadioButton("Linux");
		rdbtnO_2.setBounds(39, 85, 109, 23);
		contentPane.add(rdbtnO_2);
		
		JRadioButton rdbtnO_3 = new JRadioButton("Mac");
		rdbtnO_3.setBounds(39, 111, 109, 23);
		contentPane.add(rdbtnO_3);
		
		ButtonGroup bgroup = new ButtonGroup();
		bgroup.add(rdbtnO_1);
		bgroup.add(rdbtnO_2);
		bgroup.add(rdbtnO_3);
		
		
		JLabel eE = new JLabel("Elije tu especialidad");
		eE.setBounds(39, 156, 164, 23);
		contentPane.add(eE);
		
		JCheckBox chckbx1 = new JCheckBox("Programación");
		chckbx1.setBounds(39, 186, 97, 23);
		contentPane.add(chckbx1);
		
		JCheckBox chckbx2 = new JCheckBox("Diseño Gráfico");
		chckbx2.setBounds(39, 212, 97, 23);
		contentPane.add(chckbx2);
		
		JCheckBox chckbx3 = new JCheckBox("Administración");
		chckbx3.setBounds(39, 238, 97, 23);
		contentPane.add(chckbx3);
		
		
		JLabel lblHorasDedicadasEn = new JLabel("Horas dedicadas en el ordenador");
		lblHorasDedicadasEn.setBounds(39, 288, 164, 23);
		contentPane.add(lblHorasDedicadasEn);
		
		JSlider slider = new JSlider();
		slider.setMaximum(10);
		slider.setBounds(39, 322, 200, 26);
		contentPane.add(slider);
		
		JButton btn1 = new JButton("Enviar");
		btn1.setBounds(39, 360, 100, 26);
		contentPane.add(btn1);
		
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String msg="";  
				String msg2="";  
		        if(rdbtnO_1.isSelected()){  
		            msg="Windows";  
		        }  
		        else if(rdbtnO_2.isSelected()){  
		            msg="Linux";  
		        }  
		        else if(rdbtnO_3.isSelected()){   
		            msg="Mac";  
		        }
		        else {
		        	msg="No se ha seleccionado sistema operativo";
		        }
				
				if(chckbx1.isSelected()){  
		            msg2="Programación \n";  
		        }  
				if(chckbx2.isSelected()){  
		            msg2+="Diseño Gráfico \n";  
		        }  
				if(chckbx3.isSelected()){   
		            msg2+="Administración \n";  
		        }  	        
				
				JOptionPane.showMessageDialog(null, "Sistema Operativo: \n" + msg + "\n\n Especialidad: \n" + msg2 + "\n\n Horas dedicadas en el ordenador: \n" + slider.getValue());
				
			}
		});
		
		
	}
}
