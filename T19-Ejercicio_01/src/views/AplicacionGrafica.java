package views;

import javax.swing.JFrame;
import javax.swing.JTextField;

import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class AplicacionGrafica {

	public JFrame frmSaludador;
	private JTextField txtEscribeUnNombre;

	/**
	 * Creamos la aplicaci�n
	 */
	public AplicacionGrafica() {
		frmSaludador = new JFrame();
		frmSaludador.setTitle("Saludador");
		frmSaludador.setBounds(100, 100, 450, 300);
		frmSaludador.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSaludador.getContentPane().setLayout(null);
		
		txtEscribeUnNombre = new JTextField();
		txtEscribeUnNombre.setBounds(80, 114, 260, 30);
		txtEscribeUnNombre.setHorizontalAlignment(SwingConstants.LEFT);
		frmSaludador.getContentPane().add(txtEscribeUnNombre);
		txtEscribeUnNombre.setColumns(10);
		
		JButton btnNewButton = new JButton("\u00A1Saludar!");
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String texto = txtEscribeUnNombre.getText();
				JOptionPane.showMessageDialog(null, "�Hola " + texto + "!");
			}
		});
		btnNewButton.setBounds(173, 176, 85, 25);
		frmSaludador.getContentPane().add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("Escribe un nombre para saludar");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel.setBounds(104, 52, 216, 30);
		frmSaludador.getContentPane().add(lblNewLabel);
	}
}
