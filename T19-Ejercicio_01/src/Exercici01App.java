import java.awt.EventQueue;

import views.AplicacionGrafica;


public class Exercici01App {
	/**
	 * Iniciamos la aplicación.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AplicacionGrafica window = new AplicacionGrafica();
					window.frmSaludador.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

}
