package views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class MainViews extends JFrame{
	
	private JPanel contentPane;
	
	public MainViews() {
		setTitle("Ejercicio 04 - MiniCalculadora");
		setBounds(300, 200, 800, 830);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		
		contentPane = new JPanel();
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		JLabel labels[] = new JLabel[3];
		JTextField textFields[] = new JTextField[3];
		
		//Operador 1
		labels[0] = new JLabel("Operador 1:");
		labels[0].setBounds(0, 0, 300, 25);
		contentPane.add(labels[0]);
		
		textFields[0] = new JTextField();
		textFields[0].setBounds(0, 20, 300, 40);
		contentPane.add(textFields[0]);
		
		//Operador 2
		labels[1] = new JLabel("Operador 2:");
		labels[1].setBounds(0, 60, 300, 25);
		contentPane.add(labels[1]);
		
		textFields[1] = new JTextField();
		textFields[1].setBounds(0, 80, 300, 40);
		contentPane.add(textFields[1]);
		
		//Resultado
		labels[2] = new JLabel("Resultado:");
		labels[2].setBounds(0, 120, 300, 25);
		contentPane.add(labels[2]);
		
		textFields[2] = new JTextField();
		textFields[2].setBounds(0, 140, 300, 40);
		textFields[2].setEnabled(false);
		contentPane.add(textFields[2]);
		
		JButton btns[] = new JButton[7];
		
		//Botón sumar
		btns[0] = new JButton("+");
		btns[0].setBounds(40, 190, 100, 40);
		contentPane.add(btns[0]);
		
		//Botón restar
		btns[1] = new JButton("-");
		btns[1].setBounds(150, 190, 100, 40);
		contentPane.add(btns[1]);
		
		//Botón multiplicar
		btns[2] = new JButton("*");
		btns[2].setBounds(40, 240, 100, 40);
		contentPane.add(btns[2]);
		
		//Botón dividir
		btns[3] = new JButton("/");
		btns[3].setBounds(150, 240, 100, 40);
		contentPane.add(btns[3]);
		
		//Botón vaciar los campos
		btns[4] = new JButton("C");
		btns[4].setBounds(40, 290, 100, 40);
		contentPane.add(btns[4]);

		//Botón cerrar aplicacion
		btns[5] = new JButton("X");
		btns[5].setBounds(150, 290, 100, 40);
		contentPane.add(btns[5]);
		
		//Botón about
		btns[6] = new JButton("About");
		btns[6].setBounds(40, 340, 200, 40);
		contentPane.add(btns[6]);
		
		//Evento sumar
		ActionListener alBtnSuma = new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				double operator1 = Double.parseDouble(textFields[0].getText());
				double operator2 = Double.parseDouble(textFields[1].getText());
				double resultado = operator1 + operator2;
				String resultadoString = Double.toString(resultado);
				textFields[2].setText(resultadoString);
			}
		};
		
		btns[0].addActionListener(alBtnSuma);
		
		//Evento restar
		ActionListener alBtnResta = new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				double operator1 = Double.parseDouble(textFields[0].getText());
				double operator2 = Double.parseDouble(textFields[1].getText());
				double resultado = operator1 - operator2;
				String resultadoString = Double.toString(resultado);
				textFields[2].setText(resultadoString);
			}
		};
		
		btns[1].addActionListener(alBtnResta);
		
		//Evento multiplicar
		ActionListener alBtnMultiplicacion = new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				double operator1 = Double.parseDouble(textFields[0].getText());
				double operator2 = Double.parseDouble(textFields[1].getText());
				double resultado = operator1 * operator2;
				String resultadoString = Double.toString(resultado);
				textFields[2].setText(resultadoString);
			}
		};
		
		btns[2].addActionListener(alBtnMultiplicacion);
		
		//Evento dividir
		ActionListener alBtnDivision = new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				double operator1 = Double.parseDouble(textFields[0].getText());
				double operator2 = Double.parseDouble(textFields[1].getText());
				double resultado = operator1 / operator2;
				String resultadoString = Double.toString(resultado);
				textFields[2].setText(resultadoString);
			}
		};
		
		btns[3].addActionListener(alBtnDivision);
		
		//Evento vaciar los campos
		ActionListener alBtnC = new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				textFields[0].setText("");
				textFields[1].setText("");
				textFields[2].setText("");
			}
		};
		
		btns[4].addActionListener(alBtnC);
			
		//Evento cerrar aplicacion
		ActionListener alBtnClose = new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				dispose();
			}
		};
		
		btns[5].addActionListener(alBtnClose);
		
		//Evento about
		ActionListener alBtnAbout = new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Este programa es una mini calculadora con JAVA");
			}
		};
		
		btns[6].addActionListener(alBtnAbout);
	}

}
