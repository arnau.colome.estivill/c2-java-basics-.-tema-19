package views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainViews extends JFrame{
	
	private JPanel contentPane;
	
	public MainViews() {
		setTitle("Ejercicio 02");
		setBounds(400, 200, 600, 300);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		contentPane = new JPanel();
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		JLabel labels[] = new JLabel[2]; 
		
		labels[0] = new JLabel("Escribe el titulo de una pelicula");
		labels[0].setBounds(60, 20, 180, 20);
		contentPane.add(labels[0]);
		
		JTextField textField = new JTextField();
		textField.setBounds(70, 50, 150, 20);
		contentPane.add(textField);
		
		JButton btn = new JButton("A�adir");
		btn.setBounds(70, 85, 150, 20);
		contentPane.add(btn);
		
		labels[1] = new JLabel("Peliculas");
		labels[1].setBounds(350, 20, 70, 20);
		contentPane.add(labels[1]);
		
		JComboBox comboBox = new JComboBox<>();
		comboBox.setBounds(350, 50, 150, 20);
		contentPane.add(comboBox);
		comboBox.addItem("Quantum of Solace");
		comboBox.addItem("Avatar");
		
		ActionListener alBtn = new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				
				comboBox.addItem(textField.getText());
				
			}
		};
		
		btn.addActionListener(alBtn);
	}

}
